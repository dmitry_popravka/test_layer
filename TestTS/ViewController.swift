//
//  ViewController.swift
//  TestTS
//
//  Created by mac-15 on 2/19/19.
//  Copyright © 2019 mac-15. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var circularTSView: CircularTSView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        circularTSView.delegate = self
    }


}

extension ViewController : CircularTSViewDelegate {
    
    func circular(view: CircularTSView, selectIdx: Set<Int>) {
        debugPrint(#function, selectIdx)
    }
}
