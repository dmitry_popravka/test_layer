//
//  LoadFromXibProtocol.swift
//  TestTS
//
//  Created by mac-15 on 2/19/19.
//  Copyright © 2019 mac-15. All rights reserved.
//

import UIKit



protocol LoadFromXibProtocol: class {}


extension LoadFromXibProtocol where Self: UIView {
    
    func loadFromXib(_ nibName: String, confNibView: ((UIView) ->())? = nil) {
        
        let nibView = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)!.first as! UIView
        
        nibView.frame = CGRect (x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        confNibView?(nibView)
        self.addSubview(nibView)
    }
}

