//
//  CircularTSView.swift
//  TestTS
//
//  Created by mac-15 on 2/19/19.
//  Copyright © 2019 mac-15. All rights reserved.
//

import UIKit

protocol CircularTSViewDelegate: NSObjectProtocol {
    func circular(view: CircularTSView, selectIdx: Set<Int>)
}

class CircularTSView: UIView, LoadFromXibProtocol {
    
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var contentView: UIView!

    private var shapeLayer: [CAShapeLayer] = []
    private var textLayers: [CATextLayer] = []
    
    var widthValue: CGFloat = 70
    var shapeLayerColor = UIColor(intRed: 255, intGreen: 194, intBlue: 194)
    var shapeLayerSelectColor = UIColor(intRed: 240, intGreen: 93, intBlue: 125)
    var textLayerColor = UIColor(intRed: 33, intGreen: 33, intBlue: 33)
    var countInnerScore: Int = 5
    var selectIdx: Set<Int> = []
    
    weak var delegate: CircularTSViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        loadFromXib("CircularTSView") { _ in
            DispatchQueue.main.async {
                self.draw()
            }
        }
    }
    
    func draw() {
        
        if countInnerScore < 1 || countInnerScore > 6 {
            assert(false, "Inner score count from 1 to 6")
            return
        }
        
        for i in 0..<countInnerScore {
            
            /// CAShapeLayer
            
            let startAngleValue: CGFloat = {
                if countInnerScore == 1 {
                    return getRadians(degrees: 0)
                } else {
                    let delta: CGFloat = CGFloat(360)/CGFloat(countInnerScore)
                    return getRadians(degrees: (delta * CGFloat(i)) + 0.5)
                }
            } ()
            
            let endAngleValue: CGFloat = {
                if countInnerScore == 1 {
                    return getRadians(degrees: 360)
                } else {
                    let delta: CGFloat = CGFloat(360)/CGFloat(countInnerScore)
                    return getRadians(degrees: (delta * CGFloat(i + 1)) - 0.5)
                }
            } ()
            
            let circlePath = UIBezierPath(arcCenter: CGPoint(x: self.contentView.frame.size.width / 2.0,
                                                             y: self.contentView.frame.size.height / 2.0),
                                          radius: getRadius(),
                                          startAngle: startAngleValue,
                                          endAngle: endAngleValue,
                                          clockwise: true)
            
            let circleLayer = CAShapeLayer()
            circleLayer.path = circlePath.cgPath
            circleLayer.fillColor = UIColor.clear.cgColor

            if selectIdx.contains(i) {
                circleLayer.strokeColor = shapeLayerSelectColor.cgColor
            } else {
                circleLayer.strokeColor = shapeLayerColor.cgColor
            }
            
            circleLayer.lineWidth = widthValue
            
            self.contentView.layer.addSublayer(circleLayer)
            self.shapeLayer.append(circleLayer)
            
            // CATextLayer

            let attributes = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12),
                NSAttributedString.Key.foregroundColor: textLayerColor
            ]
            
            let text = checkingMaxText(str: "test", ofFont: UIFont.systemFont(ofSize: 12))
            
            let attributedString = NSAttributedString(string: "\(i) \(text)", attributes: attributes )

            let textLayer = CATextLayer()
            textLayer.string = attributedString
            textLayer.frame = self.contentView.bounds

            let radians: CGFloat = {
                
                if countInnerScore == 1 {
                    return getTextRadians(degrees: 0)
                } else {
                    let delta: CGFloat = CGFloat(360)/CGFloat(countInnerScore)
                    return getTextRadians(degrees: ((delta * CGFloat(i + 1) ) - (delta / 2)))
                }
            } ()

            textLayer.transform = CATransform3DMakeAffineTransform(CGAffineTransform(rotationAngle: radians));
            textLayer.alignmentMode = .center
            textLayer.anchorPoint = CGPoint(x: 0.5, y: 0.5)

            self.contentView.layer.insertSublayer(textLayer, at: 0)

            self.textLayers.append(textLayer)
          
        }
    }
        
  
    func clearAll() {
        
        /// shapeLayer
        self.shapeLayer.forEach {
            $0.removeFromSuperlayer()
        }
        
        shapeLayer.removeAll()

        /// textLayer
        self.textLayers.forEach {
            $0.removeFromSuperlayer()
        }
        
        textLayers.removeAll()
    }
    
    //MARK: -
    //MARK: touches

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesBegan(touches, with: event)
        
        guard let point = touches.first?.location(in: self.contentView) else { return }
        
        let distance = distanceToPoint(otherPoint: point)
        let radius = getRadius()
        if distance >= (radius - widthValue/2) &&  distance <= (radius + widthValue/2) {
            
            let delta: CGFloat = CGFloat(360)/CGFloat(countInnerScore)
            
            for i in 1...countInnerScore {
                
                let startAngle = delta * CGFloat(i)
                let endAngle = delta * CGFloat(i + 1)
                let angle = getDegrees(radians: angleToPoint(pointOnCircle: point))
                
                if startAngle >= angle && angle <= endAngle {
                    let item = i - 1
                    if selectIdx.contains(item) {
                        selectIdx.remove(item)
                    } else {
                        selectIdx.insert(item)
                    }
                    
                    DispatchQueue.main.async {
                        self.clearAll()
                        self.draw()
                        self.delegate?.circular(view: self, selectIdx: self.selectIdx)
                    }
                    return
                }
            }
        }
    }
    
    //MARK: -
    //MARK: private
    
    
    private func maxLengthTextLayer() -> Double {
        
        let count : Int = {
            if countInnerScore == 1 {
                return 2
            } else {
                return countInnerScore
            }
        }()
        
        let length = Double(getRadius()) * cos(Double.pi * Double(0.5 - (1 / Double(count)))) * Double(2) * Double(0.95)
        
        return length
    }

    
    func checkingMaxText(str: String, ofFont font: UIFont) -> String {
        
        let maxWidthLayer = maxLengthTextLayer()
        
        
        func size(text: String, ofFont font: UIFont) -> CGSize {
            return (text as NSString).size(withAttributes: [NSAttributedString.Key.font: font])
        }
        
        let returnText: String = {
            
            var text = str
            
            while CGFloat(maxWidthLayer) < size(text: text, ofFont: font).width {
                text = String(text.dropLast())
            }
            
            return text
        } ()
        
        return returnText
    }

    private func getRadius() -> CGFloat {
        let radiusValue: CGFloat = self.contentView.frame.size.width / 2
        return radiusValue - widthValue / 1.2
    }
    
    private func getRadians(degrees: CGFloat) -> CGFloat {
        return CGFloat(degrees * CGFloat.pi / 180) - CGFloat(Double.pi / 2.0)
    }
    
    private func getTextRadians(degrees: CGFloat) -> CGFloat {
        return CGFloat(degrees * CGFloat.pi / 180)
    }
    
    private func getDegrees(radians: CGFloat) -> CGFloat {
        return radians * 180 / CGFloat.pi
    }
    
    private func angleToPoint(pointOnCircle: CGPoint) -> CGFloat {
        
        let center = CGPoint(x: self.contentView.frame.size.width / 2.0,
                             y: self.contentView.frame.size.height / 2.0)
        
        let originX = pointOnCircle.x - center.x
        let originY = pointOnCircle.y - center.y
        var radians = atan2(originY, originX) + CGFloat(Double.pi / 2.0)
        
        while radians < 0 {
            radians += CGFloat(2 * Double.pi)
        }
        
        return radians
    }
    
    private func distanceToPoint(otherPoint: CGPoint) -> CGFloat {
        
        let center = CGPoint(x: self.contentView.frame.size.width / 2.0,
                             y: self.contentView.frame.size.height / 2.0)
        
        return sqrt(pow((otherPoint.x - center.x), 2) + pow((otherPoint.y - center.y), 2))
    }
}
